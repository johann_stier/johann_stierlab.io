Based on https://gitlab.com/danielo515/tw5-auto-publish2gitlab-pages

Publish new version:

* import final file into local node-based version of tiddlywiki
* push resulting files to the repo.
* copy system files into wiki/system and rename with names of files already there.